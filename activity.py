year = int(input('Please input a year: '))
 
if ( year < 0 ) or ( year == 0 ) :
    print("Not allowed, please enter year")
elif ( year%4 == 0 and year%100 != 0 ) or ( year%400 == 0 ) :
    print(f"{year} is a leap year.")
else :
    print(f"{year} is not a leap year.")


rows = int(input("Enter number of rows: \n"))
col = int(input("Enter number of columns: \n"))
for i in range(rows):
    print (' *' * col + ' ')